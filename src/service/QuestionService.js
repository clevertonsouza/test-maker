import axios from 'axios'
import { WS_QUESTIONS_BY_TEST } from './ServiceEndpoints'

export default class QuestionService {

    async findByTest(testId) {
        let url = WS_QUESTIONS_BY_TEST + testId
        let result = await axios.get(url)
        return result.data
    }
}